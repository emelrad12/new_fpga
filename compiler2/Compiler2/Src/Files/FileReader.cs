﻿using System.IO;

namespace Compiler2.Files
{
    public static class FileReader
    {
        public static string programDirectory = "../../../ProgramSource/";
        public static string programSuffix = ".nlg";

        public static string Read(string fileName)
        {
            return File.ReadAllText(ConvertName(fileName));
        }

        public static void Write(string fileName, string data)
        {
            File.WriteAllText(fileName, ConvertName(data));
        }

        private static string ConvertName(string name)
        {
            return programDirectory + name + programSuffix;
        }
    }
}