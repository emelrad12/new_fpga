﻿using System;
using System.Collections.Generic;
using Compiler2.Parser;
using Compiler2.Scanner;

namespace Compiler2.Interpreter
{
    public class Interpreter : Expr.Visitor<object>, Stmt.Visitor<object>
    {
        public Interpreter()
        {
            globals.Define("clock", new NativeFunctions.NativeFunction_Clock());
            _environment = globals;
        }

        public class RuntimeError : Exception
        {
            public Token token;

            public RuntimeError(Token token, String message) : base(message)
            {
                this.token = token;
            }
        }

        public void Interpret(List<Stmt> statements, bool repl = false)
        {
            _repl = repl;
            try
            {
                foreach (Stmt statement in statements)
                {
                    Execute(statement);
                }
            }
            catch (RuntimeError error)
            {
                Ngl.RuntimeError(error);
            }
        }

        public void Resolve(Expr expr, int depth)
        {
            _locals.Add(expr, depth);
        }

        public object VisitExpressionStmt(Stmt.Expression stmt)
        {
            return Evaluate(stmt.expression);
        }

        public object VisitPrintStmt(Stmt.Print stmt)
        {
            Object value = Evaluate(stmt.expression);
            Console.WriteLine(Stringify(value));
            return null;
        }

        public object VisitReturnStmt(Stmt.Return stmt)
        {
            Object value = null;
            if (stmt.value != null) value = Evaluate(stmt.value);

            throw new Return(value);
        }

        public object VisitVarStmt(Stmt.Var stmt)
        {
            Object value = new InvalidObject();
            if (stmt.initializer != null)
            {
                value = Evaluate(stmt.initializer);
            }

            _environment.Define(stmt.name.lexeme, value);
            return null;
        }

        public object VisitVariableExpr(Expr.Variable expr)
        {
            return LookUpVariable(expr.name, expr);
        }

        private Object LookUpVariable(Token name, Expr expr)
        {
            if (_locals.ContainsKey(expr))
            {
                int distance = _locals[expr];
                return _environment.GetAt(distance, name.lexeme);
            }
            else
            {
                return globals.Get(name);
            }
        }
        
        public object VisitClassStmt(Stmt.Class stmt) {     
            _environment.Define(stmt.name.lexeme, null);     
            LoxClass klass = new LoxClass(stmt.name.lexeme);
            _environment.Assign(stmt.name, klass);           
            return null;                                    
        }         
        
        public object VisitAssignExpr(Expr.Assign expr)
        {
            Object value = Evaluate(expr.value);
            if (_locals.ContainsKey(expr))
            {
                var distance = _locals[expr];
                _environment.AssignAt(distance, expr.name, value);
            }
            else
            {
                globals.Assign(expr.name, value);
            }

            _environment.Assign(expr.name, value);
            return value;
        }

        public object VisitBinaryExpr(Expr.Binary expr)
        {
            object left = Evaluate(expr.left);
            object right = Evaluate(expr.right);

            switch (expr.operatorItem.type)
            {
                case TokenType.Minus:
                    CheckNumberOperands(expr.operatorItem, left, right);
                    return (double) left - (double) right;
                case TokenType.Slash:
                    CheckNumberOperands(expr.operatorItem, left, right);
                    if ((double) right == 0)
                    {
                        Ngl.Warn("Division by zero");
                    }

                    return (double) left / (double) right;
                case TokenType.Star:
                    CheckNumberOperands(expr.operatorItem, left, right);
                    return (double) left * (double) right;
                case TokenType.Plus:
                {
                    if (left is double && right is double)
                    {
                        return (double) left + (double) right;
                    }

                    if (left is string && right is string)
                    {
                        return (string) left + (string) right;
                    }

                    throw new RuntimeError(expr.operatorItem,
                        "Operands must be two numbers or two strings.");
                }
                case TokenType.Greater:
                    CheckNumberOperands(expr.operatorItem, left, right);
                    return (double) left > (double) right;
                case TokenType.GreaterEqual:
                    CheckNumberOperands(expr.operatorItem, left, right);
                    return (double) left >= (double) right;
                case TokenType.Less:
                    CheckNumberOperands(expr.operatorItem, left, right);
                    return (double) left < (double) right;
                case TokenType.LessEqual:
                    CheckNumberOperands(expr.operatorItem, left, right);
                    return (double) left <= (double) right;
                case TokenType.BangEqual: return !IsEqual(left, right);
                case TokenType.EqualEqual: return IsEqual(left, right);
            }

            // Unreachable.                                
            return null;
        }

        public Object VisitCallExpr(Expr.Call expr)
        {
            Object callee = Evaluate(expr.callee);
            if (!(callee is ILoxCallable))
            {
                throw new RuntimeError(expr.paren,
                    "Can only call functions and classes.");
            }

            List<Object> arguments = new List<Object>();
            foreach (Expr argument in expr.arguments)
            {
                arguments.Add(Evaluate(argument));
            }

            ILoxCallable function = (ILoxCallable) callee;
            if (arguments.Count != function.Arity())
            {
                throw new RuntimeError(expr.paren, "Expected " +
                                                   function.Arity() + " arguments but got " +
                                                   arguments.Count + ".");
            }


            return function.Call(this, arguments);
        }

        private void Execute(Stmt stmt)
        {
            var a = stmt.Accept(this);
            if (_repl)
            {
                Console.WriteLine(a);
            }
        }

        public object VisitFunctionStmt(Stmt.Function stmt)
        {
            LoxFunction function = new LoxFunction(stmt, _environment);
            _environment.Define(stmt.name.lexeme, function);
            return null;
        }

        public object VisitIfStmt(Stmt.If stmt)
        {
            if (IsTruthy(Evaluate(stmt.condition)))
            {
                Execute(stmt.thenBranch);
            }
            else if (stmt.elseBranch != null)
            {
                Execute(stmt.elseBranch);
            }

            return null;
        }

        public object VisitWhileStmt(Stmt.While stmt)
        {
            while (IsTruthy(Evaluate(stmt.condition)))
            {
                Execute(stmt.body);
            }

            return null;
        }

        public object VisitBlockStmt(Stmt.Block stmt)
        {
            ExecuteBlock(stmt.statements, new Environment(_environment));
            return null;
        }

        public void ExecuteBlock(List<Stmt> statements, Environment environment)
        {
            Environment previous = _environment;
            try
            {
                _environment = environment;

                foreach (Stmt statement in statements)
                {
                    Execute(statement);
                }
            }
            finally
            {
                _environment = previous;
            }
        }

        private String Stringify(object item)
        {
            if (item == null) return "nil";
            if (item is Double)
            {
                String text = item.ToString();
                if (text.EndsWith(".0"))
                {
                    text = text.Substring(0, text.Length - 2);
                }

                return text;
            }

            return item.ToString();
        }

        private void CheckNumberOperands(Token operatorItem, Object left, Object right)
        {
            if (left is Double && right is Double) return;
            throw new RuntimeError(operatorItem, "Operands must be numbers.");
        }

        public object VisitGroupingExpr(Expr.Grouping expr)
        {
            return Evaluate(expr.expression);
        }

        public object VisitLiteralExpr(Expr.Literal expr)
        {
            return expr.value;
        }

        public object VisitLogicalExpr(Expr.Logical expr)
        {
            Object left = Evaluate(expr.left);

            if (expr.operatorItem.type == TokenType.Or)
            {
                if (IsTruthy(left)) return left;
            }
            else
            {
                if (!IsTruthy(left)) return left;
            }

            return Evaluate(expr.right);
        }

        public object VisitUnaryExpr(Expr.Unary expr)
        {
            Object right = Evaluate(expr.right);
            switch (expr.operatorItem.type)
            {
                case TokenType.Minus:
                    CheckNumberOperand(expr.operatorItem, right);
                    return -(double) right;
                case TokenType.Bang:
                    return !IsTruthy(right);
            }

            // Unreachable.                              
            return null;
        }


        private object Evaluate(Expr expr)
        {
            return expr.Accept(this);
        }

        private bool IsTruthy(object item)
        {
            if (item == null) return false;
            if (item is bool b) return b;
            return true;
        }

        private bool IsEqual(object a, object b)
        {
            // nil is only equal to nil.               
            if (a == null && b == null) return true;
            if (a == null) return false;

            return a.Equals(b);
        }

        private void CheckNumberOperand(Token operatorItem, Object operand)
        {
            if (operand is double) return;
            throw new RuntimeError(operatorItem, "Operand must be a number.");
        }

        private Dictionary<Expr, int> _locals = new Dictionary<Expr, int>();
        private bool _repl = false;
        public Environment globals = new Environment();
        private Environment _environment;
    }
}