﻿using System.Collections.Generic;
using Compiler2.Scanner;

namespace Compiler2.Interpreter
{
    public class InvalidObject
    {
    }

    public class Environment
    {
        private Dictionary<string, object> _values = new Dictionary<string, object>();
        private Environment _enclosing;

        public Environment()
        {
            _enclosing = null;
        }

        public Environment(Environment enclosing)
        {
            _enclosing = enclosing;
        }

        public void Define(string name, object value)
        {
            _values.Add(name, value);
        }

        public object GetAt(int distance, string name)
        {
            Environment en = Ancestor(distance);
            return Ancestor(distance)._values[name];
        }

        public void AssignAt(int distance, Token name, object value)
        {
            Ancestor(distance)._values[name.lexeme] =  value;
        }

        Environment Ancestor(int distance)
        {
            Environment environment = this;
            for (int i = 0; i < distance; i++)
            {
                environment = environment._enclosing;
            }

            return environment;
        }

        public object Get(Token name)
        {
            if (_values.ContainsKey(name.lexeme))
            {
                var item = _values[name.lexeme];
                if (item is InvalidObject)
                {
                    throw new Interpreter.RuntimeError(name, "Cannot read unassigned variable '" + name.lexeme + "'.");
                }

                return _values[name.lexeme];
            }

            if (_enclosing != null) return _enclosing.Get(name);

            throw new Interpreter.RuntimeError(name, "Undefined variable '" + name.lexeme + "'.");
        }

        public void Assign(Token name, object value)
        {
            if (_values.ContainsKey(name.lexeme))
            {
                _values[name.lexeme] = value;
                return;
            }

            if (_enclosing != null)
            {
                _enclosing.Assign(name, value);
                return;
            }

            throw new Interpreter.RuntimeError(name,
                "Undefined variable '" + name.lexeme + "'.");
        }
    }
}