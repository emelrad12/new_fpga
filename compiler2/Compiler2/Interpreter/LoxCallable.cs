﻿using System.Collections.Generic;

namespace Compiler2.Interpreter
{
    public interface ILoxCallable
    {
        public object Call(Interpreter interpreter, List<object> arguments);
        public int Arity();
    }
}