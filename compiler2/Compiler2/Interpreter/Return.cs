﻿using System;

namespace Compiler2.Interpreter
{
    class Return : Exception
    {
        public object value;

        public Return(object value)
        {
            this.value = value;
        }
    }
}