﻿using System;
using System.Collections.Generic;

namespace Compiler2.Interpreter.NativeFunctions
{
    public class NativeFunction_Clock : ILoxCallable
    {
        public object Call(Interpreter interpreter, List<object> arguments)
        {
            return DateTime.Now.Second;
        }

        public int Arity()
        {
            return 0;
        }
    }
}