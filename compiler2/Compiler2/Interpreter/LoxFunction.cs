﻿using System;
using System.Collections.Generic;
using Compiler2.Parser;

namespace Compiler2.Interpreter
{
    class LoxFunction : ILoxCallable
    {
        private Stmt.Function _declaration;
        private Environment _closure;

        public LoxFunction(Stmt.Function declaration, Environment closure)
        {
            _closure = closure;
            _declaration = declaration;
        }

        public object Call(Interpreter interpreter, List<object> arguments)
        {
            Environment environment = new Environment(_closure);
            for (int i = 0; i < _declaration.paramItems.Count; i++)
            {
                environment.Define(_declaration.paramItems[i].lexeme,
                    arguments[i]);
            }

            try
            {
                interpreter.ExecuteBlock(_declaration.body, environment);
            }
            catch (Return returnValue)
            {
                return returnValue.value;
            }

            return null;
        }

        public int Arity()
        {
            return _declaration.paramItems.Count;
        }

        public override String ToString()
        {
            return "<fn " + _declaration.name.lexeme + ">";
        }
    }
}