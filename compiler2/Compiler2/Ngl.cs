﻿using System;
using System.Collections.Generic;
using Compiler2.Files;
using Compiler2.Interpreter;
using Compiler2.Parser;
using Compiler2.Scanner;
using Environment = System.Environment;

namespace Compiler2
{
    public static class Ngl
    {
        private static Interpreter.Interpreter interpreter = new Interpreter.Interpreter();
        private static bool hadError = false;
        private static bool hadRuntimeError = false;

        public static void NglMain(String[] args)
        {
            if (args.Length > 1)
            {
                Console.WriteLine("Usage: jlox [script]");
                Environment.Exit(0);
            }
            else if (args.Length == 1)
            {
                RunFile(args[0]);
            }
            else
            {
                RunPrompt();
            }
        }

        private static void RunFile(String path)
        {
            Run(FileReader.Read(path), false);
            if (hadError) Environment.Exit(65);
            if (hadRuntimeError) Environment.Exit(70);
        }

        private static void RunPrompt()
        {
            while (true)
            {
                Console.WriteLine("> ");
                Run(Console.ReadLine(), true);
                hadError = false;
            }
        }

        private static void Run(String source, bool repl)
        {
            Scanner.Scanner scanner = new Scanner.Scanner(source);
            List<Token> tokens = scanner.ScanTokens();
            Parser.Parser parser = new Parser.Parser(tokens, repl);
            List<Stmt> statements = parser.Parse();
            if (hadError) return;
            Resolver resolver = new Resolver(interpreter);
            resolver.Resolve(statements);
            if (hadError) return;
            interpreter.Interpret(statements, repl);
        }

        public static void RuntimeError(Interpreter.Interpreter.RuntimeError error)
        {
            Console.Write(error.Message + "\n[line " + error.token.line + "]");
            hadRuntimeError = true;
        }

        public static void Report(int line, string where, string message)
        {
            hadError = true;
            Console.Write("[line " + line + "] Error" + where + " : " + message);
        }

        public static void ThrowError(int line, int start, String message)
        {
            Report(line, start.ToString(), message);
        }

        public static void ThrowError(Token start, String message)
        {
            Report(0, start.ToString(), message);
        }

        public static void Error(Token token, string message)
        {
            if (token.type == TokenType.Eof)
            {
                Report(token.line, " at end", message);
            }
            else
            {
                Report(token.line, " at '" + token.lexeme + "'", message);
            }
        }

        public static void Warn(string item)
        {
            Console.WriteLine("Warning: " + item);
        }
    }
}