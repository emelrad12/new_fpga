﻿using System.Collections.Generic;

namespace Compiler2.Scanner
{
    public static class Keywords
    {
        public static Dictionary<string, TokenType> keywords = new Dictionary<string, TokenType>();

        static Keywords()
        {
            keywords.Add("and", TokenType.And);
            keywords.Add("class", TokenType.Class);
            keywords.Add("else", TokenType.Else);
            keywords.Add("false", TokenType.False);
            keywords.Add("for", TokenType.For);
            keywords.Add("fun", TokenType.Fun);
            keywords.Add("if", TokenType.If);
            keywords.Add("nil", TokenType.Nil);
            keywords.Add("or", TokenType.Or);
            keywords.Add("print", TokenType.Print);
            keywords.Add("return", TokenType.Return);
            keywords.Add("super", TokenType.Super);
            keywords.Add("this", TokenType.This);
            keywords.Add("true", TokenType.True);
            keywords.Add("var", TokenType.Var);
            keywords.Add("while", TokenType.While);
        }
    }
}