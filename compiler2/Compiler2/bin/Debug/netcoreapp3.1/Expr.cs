namespace Compiler2.Parser
using System.Collections.Generic
abstract class Expr {
  static class Binary extends Expr {
    Binary(Expr left, Token operator, Expr right) {
      this.left = left;
      this.operator = operator;
      this.right = right;
    }
     Expr left;
     Token operator;
     Expr right;
  }
  static class Grouping extends Expr {
    Grouping(Expr expression) {
      this.expression = expression;
    }
     Expr expression;
  }
  static class Literal extends Expr {
    Literal(Object value) {
      this.value = value;
    }
     Object value;
  }
  static class Unary extends Expr {
    Unary(Token operator, Expr right) {
      this.operator = operator;
      this.right = right;
    }
     Token operator;
     Expr right;
  }
}
