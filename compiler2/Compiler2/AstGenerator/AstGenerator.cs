﻿using System;
using System.IO;

namespace Compiler2.AstGenerator
{
    public class AstGenerator
    {
        public string result = "";

        public AstGenerator()
        {
            string outputDir = "../../../Parser";
            DefineAst(outputDir, "Expr",
                "Assign   : Token name, Expr value",
                "Binary   : Expr left, Token operatorItem, Expr right",
                "Call     : Expr callee, Token paren, List<Expr> arguments",
                "Grouping : Expr expression",
                "Literal  : object value",
                "Logical  : Expr left, Token operatorItem, Expr right",
                "Unary    : Token operatorItem, Expr right",
                "Variable : Token name"
            );
            DefineAst(outputDir, "Stmt",
                "Function   : Token name, List<Token> paramItems, List<Stmt> body",
                "If         : Expr condition, Stmt thenBranch, Stmt elseBranch",
                "While      : Expr condition, Stmt body",
                "Block      : List<Stmt> statements",
                "Class      : Token name, List<Stmt.Function> methods",
                "Expression : Expr expression",
                "Print      : Expr expression",
                "Return     : Token keyword, Expr value",
                "Var        : Token name, Expr initializer"
            );
        }

        public void DefineAst(string outputDir, string baseName, params string[] types)
        {
            result = "";
            AddLine("using System.Collections.Generic;");
            AddLine("using Compiler2.Scanner;");
            AddLine("namespace Compiler2.Parser");
            AddLine("{");
            AddLine("public abstract class " + baseName + " {");
            DefineVisitor(baseName, types);

            foreach (string type in types)
            {
                string className = type.Split(":")[0].Trim();
                string fields = type.Split(":")[1].Trim();
                DefineType(baseName, className, fields);
            }

            AddLine(" public abstract  R Accept<R>(Visitor<R> visitor);");
            AddLine("}");
            AddLine("}");
            File.WriteAllText(outputDir + "/" + baseName + ".cs", result);
        }

        private void DefineType(string baseName,
            string className, string fieldList)
        {
            AddLine("  public class " + className + " : " +
                    baseName + " {");

            // Constructor.                                              
            AddLine("  public  " + className + "(" + fieldList + ") {");

            // Store parameters in fields.                               
            string[] fields = fieldList.Split(", ");
            foreach (string field in fields)
            {
                string name = field.Split(" ")[1];
                AddLine("      this." + name + " = " + name + ";");
            }

            AddLine("    }");
            AddLine("     public override R Accept<R>(Visitor<R> visitor) {");
            AddLine("      return visitor.Visit" + className + baseName + "(this);");
            AddLine("    }");
            foreach (string field in fields)
            {
                AddLine("     public " + field + ";");
            }

            AddLine("  }");
        }

        private void DefineVisitor(string baseName, string[] types)
        {
            AddLine(" public interface Visitor<R> {");

            foreach (String type in types)
            {
                String typeName = type.Split(":")[0].Trim();
                AddLine("    R Visit" + typeName + baseName + "(" +
                        typeName + " " + baseName.ToLower() + ");");
            }

            AddLine("  }");
        }

        public void AddLine(string item)
        {
            result += item;
            result += "\n";
        }
    }
}