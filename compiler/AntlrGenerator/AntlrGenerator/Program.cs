﻿using Antlr4.Runtime;

namespace AntlrGenerator
{
 
    class Program
    {
        private static void Main(string[] args)
        {
            var data = new AntlrFileStream("../../../Grammar/program.nlg");
            var lexer = new GrammarLexer(data);
        }
    }
}