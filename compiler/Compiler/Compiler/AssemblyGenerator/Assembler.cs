﻿using System;
using System.Collections.Generic;
using Compiler.Assembly;
using Compiler.Lexer;

namespace Compiler.AssemblyGenerator
{
    public class Assembler
    {
        public Assembler()
        {
            var preLexer = new PreLexerBuilder();
            preLexer.BuildPreLexDefinitions(FileReaderWriter.ReadFile(@"..\..\Assembly\LexerDefinitions.txt"));
            var lexer = new Lexer.Lexer(preLexer.lexUnitDefinitions);
            List<MatchedWord> lexedUnits = lexer.ParseText(FileReaderWriter.ReadFile(@"..\..\Assembly\AssemblyDefinitions.txt"));
            var currentWord = lexedUnits[0].word;
            var currentItems = new Dictionary<string, List<MatchedWord>>();
            currentItems[currentWord] = new List<MatchedWord>();
            for (int i = 0; i < lexedUnits.Count; i++)
            {
                var currentItem = lexedUnits[i];
                if (currentItem.word == ";" && lexedUnits.Count > i + 1)
                {
                    currentWord = lexedUnits[i + 1].word;
                    currentItems[currentWord] = new List<MatchedWord>();
                }
                else
                {
                    currentItems[currentWord].Add(currentItem);
                }
            }

            _assemblyDefinitions = currentItems;
        }

        private Dictionary<string, List<MatchedWord>> _assemblyDefinitions;

        public List<string> Assemble(List<MatchedWord> unitsToAssemble)
        {
            Dictionary<string, int> variables = new Dictionary<string, int>();
            List<string> result = new List<string>();

            for (int i = 0; i < unitsToAssemble.Count; i++)
            {
                var currentUnit = unitsToAssemble[i];
                if (IsDeclaration(unitsToAssemble, i))
                {
                    variables.Add(currentUnit.word, int.Parse(unitsToAssemble[i + 2].word));
                    i += 3;
                    continue;
                }

                if (IsAssemblyOp(unitsToAssemble, i))
                {
                    result.Add(GetAssemblyString(currentUnit.word,
                        ResolveVariables(unitsToAssemble[i + 2].word, variables),
                        ResolveVariables(unitsToAssemble[i + 4].word, variables),
                        ResolveVariables(unitsToAssemble[i + 6].word, variables)));
                    i += 8;
                    continue;
                }
            }

            return result;
        }

        public int ResolveVariables(string variable, Dictionary<string, int> variables)
        {
            if (int.TryParse(variable, out _))
            {
                return int.Parse(variable);
            }
            else
            {
                return variables[variable];
            }
        }

        public bool IsDeclaration(List<MatchedWord> unitsToAssemble, int position)
        {
            if (position == 0 || unitsToAssemble[position - 1].word == ";")
            {
                if (unitsToAssemble[position].lexUnit.type == LexicalType.Identifier && unitsToAssemble[position + 1].word == "=" &&
                    unitsToAssemble[position + 2].lexUnit.type == LexicalType.Constant && unitsToAssemble[position + 3].word == ";")
                {
                    return true;
                }
            }

            return false;
        }

        public bool IsAssemblyOp(List<MatchedWord> unitsToAssemble, int position)
        {
            int argumentCount = 0;
            if (position == 0 || unitsToAssemble[position - 1].word == ";")
            {
                for (int i = position; i < unitsToAssemble.Count; i++)
                {
                    var currentUnit = unitsToAssemble[i];
                    if (currentUnit.word == "=")
                    {
                        return false;
                    }

                    if (currentUnit.word == ";")
                    {
                        return true;
                    }
                }
            }

            return true;
        }

        private string GetAssemblyString(string instruction, int Rc, int Ra, int lastItem)
        {
            string binaryInstruction = _assemblyDefinitions[instruction][1].word;
            if (instruction.EndsWith("C"))
            {
                return binaryInstruction + ConvertNumberToBinary(Rc) + ConvertNumberToBinary(Ra) + ConvertNumberToBinary(lastItem, 16);
            }
            else
            {
                return binaryInstruction + ConvertNumberToBinary(Rc) + ConvertNumberToBinary(Ra) + ConvertNumberToBinary(lastItem, 5).PadRight(16, '0');
            }
        }

        private string ConvertNumberToBinary(int item, int width = 5)
        {
            return Convert.ToString(item, 2).PadLeft(width, '0');
        }
    }
}