﻿using System;
using System.Collections.Generic;

namespace Compiler.AssemblyGenerator
{
    public class MifGenerator
    {
        public string GenerateMif(List<string> data, int width = 32, int depth = 8196)
        {
            string result = "";
            result += "WIDTH=" + width + ";\n";
            result += "DEPTH=" + depth + ";\n";
            result += "ADDRESS_RADIX=HEX;\n";
            result += "DATA_RADIX=BIN;\n";
            result += "CONTENT BEGIN\n";
            var lastCount = 0;
            for (int i = 0; i < data.Count; i++)
            {
                lastCount = i;
                result += "	   ";
                result += FormatInHex(i);
                result += "  :  ";
                result += data[i];
                result += ";\n";
            }
            //todo figure out how depth works
            lastCount++;
            result += "	   ";
            result += FormatInHex(lastCount);
            result += "  :  ";
            result += "00000000000000000000000000000000";
            result += ";\n";
            lastCount++;
            result += "	   [" + FormatInHex(lastCount ) + "..1FFF]  :   00000000000000000000000000000000;\n";
            result += "END;";
            return result;
        }

        private string FormatInHex(int number)
        {
            return Convert.ToString(number, 16).PadLeft(4, '0');
        }
    }
}