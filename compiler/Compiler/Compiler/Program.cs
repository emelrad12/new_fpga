﻿using System;
using System.Collections.Generic;
using Compiler.Assembly;
using Compiler.AssemblyGenerator;
using Compiler.Lexer;

namespace Compiler
{
    internal static class Program
    {
        public static void Main(string[] args)
        {
            var preLexer = new PreLexerBuilder();
            preLexer.BuildPreLexDefinitions(FileReaderWriter.ReadFile(@"..\..\Assembly\LexerDefinitions.txt"));
            var lexer = new Lexer.Lexer(preLexer.lexUnitDefinitions);
            List<MatchedWord> lexedUnits = lexer.ParseText(FileReaderWriter.ReadFile(@"..\..\Assembly\Program.txt"));
            var assembler = new Assembler();
            var assembly = assembler.Assemble(lexedUnits);
            foreach (var ass in assembly)
            {
                Console.WriteLine(ass);
            }
            var mifGenerator = new MifGenerator();
            var mif = mifGenerator.GenerateMif(assembly);
            Console.WriteLine(mif);
            FileReaderWriter.WriteFile(@"..\..\..\..\..\src\Cpu\InitialMemory.mif", mif);
            FileReaderWriter.WriteFile(@"..\..\..\..\..\simulation\modelsim\src\Cpu\InitialMemory.mif", mif);
        }
    }
}