﻿using System;
using System.Collections.Generic;
using Compiler.Lexer;

namespace Compiler.Parser
{
    public class Node
    {
        public string name;
        public NodeType nodeType;
        public LexicalType lexicalType;
        public string word = String.Empty;
        public List<Node> children = new List<Node>();

        public void Print(int depth)
        {
            
            if (nodeType != NodeType.Terminal)
            {
                Console.WriteLine(new string(' ', depth * 4) + name);
            }
            else
            {
                Console.WriteLine(new string(' ', depth * 4) + name + "//" + word);
            }
            foreach (var child in children)
            {
                child.Print(depth + 1);
            }
        }
    }

    public enum NodeType
    {
        Terminal,
        Expression,
        TerminalExpression
    }
}