﻿using System.Collections.Generic;
using Compiler.Lexer;

namespace Compiler.Parser
{
    public class Parser
    {
        public bool TryParse(List<MatchedWord> listToParse, Grammar grammar)
        {
            int position = 0;
            var main = new Node()
            {
                name = "start",
                nodeType = NodeType.Expression
            };
            var matched = grammar.grammarUnits["main"].TryMatch(listToParse, ref position, main);
            main.Print(0);
            if (position != listToParse.Count)
            {
                return false;
            }

            return matched;
        }
    }
}