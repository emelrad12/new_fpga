﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Compiler.Lexer;

namespace Compiler.Parser
{
    public class Grammar
    {
        public Dictionary<string, ExpressionUnit> grammarUnits = new Dictionary<string, ExpressionUnit>();

        public void AddExpressionUnit(string name, ExpressionUnit unit)
        {
            grammarUnits.Add(name, unit);
        }
    }

    public class ExpressionUnit
    {
        public List<ExpressionUnit> expressions = new List<ExpressionUnit>();
        
        public Node AddNodeToParent(Node parentNode)
        {
            var selfNode = new Node
            {
                nodeType = nodeType,
                lexicalType = lexicalType,
                name = name
            };
            parentNode.children.Add(selfNode);
            return selfNode;
        }

        public void RemoveSelfFromParent(Node parentNode)
        {
            parentNode.children.RemoveAt(parentNode.children.Count - 1);
        }

        public bool TryMatch(List<MatchedWord> matchedWords, ref int position, Node parentNode)
        {
            var selfNode = AddNodeToParent(parentNode);
            int savedPos = position;
            switch (nodeType)
            {
                case NodeType.Expression:
                {
                    foreach (var expression in expressions)
                    {
                        if (expression.TryMatch(matchedWords, ref position, selfNode))
                        {
                            RemoveSelfFromParent(parentNode);
                            parentNode.children.AddRange(selfNode.children);
                            return true;
                        }

                        position = savedPos;
                    }

                    RemoveSelfFromParent(parentNode);
                    return false;
                }
                case NodeType.TerminalExpression:
                {
                    foreach (var expression in expressions)
                    {
                        if (!expression.TryMatch(matchedWords, ref position, selfNode))
                        {
                            position = savedPos;
                            RemoveSelfFromParent(parentNode);
                            return false;
                        }
                    }

                    return true;
                }
                case NodeType.Terminal:
                {
                    if (matchedString == "empty")
                    {
                        return true;
                    }

                    if (position == matchedWords.Count)
                    {
                        return false;
                    }

                    var t = matchedWords[position].lexUnit.type == lexicalType && Regex.Match(matchedWords[position].word, "^" + matchedString + "$").Success;
                    parentNode.children.Last().word = matchedWords[position].word;
                    position++;
                    if (!t)
                    {
                        RemoveSelfFromParent(parentNode);
                    }

                    if (lexicalType == LexicalType.Separator)
                    {
                        RemoveSelfFromParent(parentNode);
                    }

                    return t;
                }
                default: throw new Exception("Invalid node");
            }
        }

        public NodeType nodeType;
        public LexicalType lexicalType;
        public string matchedString;
        public string name;
    }
}