﻿using System;
using Compiler.Lexer;

namespace Compiler.Assembly
{
    public class PreLexerBuilder
    {
        public LexUnitDefinitions lexUnitDefinitions = new LexUnitDefinitions();

        public void BuildPreLexDefinitions(string text)
        {
            var lines = text.Split('\n');
            foreach (var line in lines)
            {
                ParseLine(line);
            }
        }

        private void ParseLine(string line)
        {
            var lineWords = line.Split(' ');
            lexUnitDefinitions.Add(new LexUnit
            {
                stringToMatch = lineWords[1],
                type = (LexicalType) Enum.Parse(typeof(LexicalType), lineWords[0]),
                omit = ArrayContainsString(lineWords, "omit")
            });
        }

        private Boolean ArrayContainsString(string[] arr, string searchedString)
        {
            foreach (var line in arr)
            {
                if (line == searchedString)
                {
                    return true;
                }
            }

            return false;
        }
        
    }
}