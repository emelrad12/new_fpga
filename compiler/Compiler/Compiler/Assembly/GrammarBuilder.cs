﻿using System;
using System.Linq;
using Compiler.Lexer;
using Compiler.Parser;

namespace Compiler.Assembly
{
    public class GrammarBuilder
    {
        public Grammar parserGrammar = new Grammar();

        public void ReadGrammar(string text)
        {
            var lines = text.Split('\n');
            foreach (var line in lines)
            {
                if (line[0] == '\\')
                {
                    continue;
                }
                PreFillUnits(line);
            }

            foreach (var line in lines)
            {
                if (line[0] == '\\')
                {
                    continue;
                }
                ParseUnitExpressions(line);
            }
        }

        private void PreFillUnits(string line)
        {
            var words = line.Split(' ');
            string nodeName = words[1];
            var nodeType = (NodeType) Enum.Parse(typeof(NodeType), words[0]);
            var unit = new ExpressionUnit
            {
                nodeType = nodeType,
                name = nodeName
            };
            if (nodeType == NodeType.Terminal)
            {
                unit.lexicalType = (LexicalType) Enum.Parse(typeof(LexicalType), words[2]);
                unit.matchedString = words[3];
            }

            parserGrammar.AddExpressionUnit(nodeName, unit);
        }

        private void ParseUnitExpressions(string line)
        {
            string[] words = line.Split(' ');
            NodeType nodeType = (NodeType) Enum.Parse(typeof(NodeType), words[0]);
            if (nodeType == NodeType.Terminal)
            {
                return;
            }

            string nodeName = words[1];
            ExpressionUnit unit = parserGrammar.grammarUnits[nodeName];
            foreach (var word in words.Skip(2))
            {
                if (!parserGrammar.grammarUnits.ContainsKey(word))
                {
                    throw new Exception("The grammar unit is missing: " + word);
                }
                unit.expressions.Add(parserGrammar.grammarUnits[word]);
            }
        }
    }
}