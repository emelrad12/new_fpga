﻿using System;
using System.Collections.Generic;

namespace Compiler.Lexer
{
    public class LexUnitDefinitions
    {
        public Dictionary<LexicalType, List<LexUnit>> lexUnits = new Dictionary<LexicalType, List<LexUnit>>();
        private int _counter;

        public void Add(LexUnit unit)
        {
                lexUnits[unit.type].Add(unit);
        }

        public LexUnitDefinitions()
        {
            foreach (LexicalType lexicalType in (LexicalType[]) Enum.GetValues(typeof(LexicalType)))
            {
                lexUnits.Add(lexicalType, new List<LexUnit>());
            }
        }
    }
}