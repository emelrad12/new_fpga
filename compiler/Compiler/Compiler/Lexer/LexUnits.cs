﻿using System.Text.RegularExpressions;

namespace Compiler.Lexer
{
    public class LexUnit
    {
        public string stringToMatch;
        public LexicalType type;
        public bool omit;
        
        public bool TryMatch(string stringToCheck)
        {
            return Regex.Match(stringToCheck, "^" + stringToMatch + "$").Success;
        }
    }

    public class MatchedWord
    {
        public MatchedWord(string word, LexUnit lexUnit)
        {
            this.word = word;
            this.lexUnit = lexUnit;
        }

        public string word;
        public LexUnit lexUnit;
    }

    public enum LexicalType
    {
        Empty,
        Separator,
        Operator,
        Identifier,
        Constant,
        Keyword
    }
}