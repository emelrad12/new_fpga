﻿using System;
using System.Collections.Generic;

namespace Compiler.Lexer
{
    public class Lexer
    {
        private string[] _textToParse;
        private string _currentWord = "";
        private LexUnitDefinitions _lexUnitDefinitions;
        private List<MatchedWord> _matchedWords = new List<MatchedWord>();

        public Lexer(LexUnitDefinitions lexUnitDefinitions)
        {
            _lexUnitDefinitions = lexUnitDefinitions;
        }

        public List<MatchedWord> ParseText(string textToParse)
        {
            foreach (char character in textToParse)
            {
                var str = character.ToString();

                if (!IsSeparator(str, out _))
                {
                    _currentWord += character;
                }
                else
                {
                    if (_currentWord.Length != 0)
                    {
                        Next();
                    }

                    _currentWord += character;
                    Next();
                }
            }

            if (_currentWord != "")
            {
                Next();
                Console.WriteLine("Warning: Expected separator at end of file");
            }

            return _matchedWords;
        }

        private void Next()
        {
            TryTokenize(_currentWord);
            _currentWord = "";
        }

        private bool IsSeparator(string character, out LexUnit whichUnit)
        {
            if (character.Length == 0)
            {
                throw new Exception("Len cannot be 0");
            }

            foreach (var type in new List<LexicalType> {LexicalType.Operator, LexicalType.Separator})
            {
                foreach (var separator in _lexUnitDefinitions.lexUnits[type])
                {
                    if (separator.TryMatch(character))
                    {
                        whichUnit = separator;
                        return true;
                    }
                }
            }

            whichUnit = null;
            return false;
        }

        private void TryTokenize(string word)
        {
            {
                if (IsSeparator(word, out LexUnit lexUnit) && !lexUnit.omit)
                {
                    var matchedWord = new MatchedWord(word, lexUnit);
                    _matchedWords.Add(matchedWord);
                    return;
                }
            }

            foreach (var unitList in _lexUnitDefinitions.lexUnits.Values)
            {
                foreach (var lexUnit in unitList)
                {
                    var isMatch = lexUnit.TryMatch(word);
                    if (isMatch)
                    {
                        var matchedWord = new MatchedWord(word, lexUnit);
                        if (!matchedWord.lexUnit.omit)
                        {
                            _matchedWords.Add(matchedWord);
                        }

                        return;
                    }
                }
            }

            throw new Exception("Parse error token does not exist:" + word);
        }
    }
}