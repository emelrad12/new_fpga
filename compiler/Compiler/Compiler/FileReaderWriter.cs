﻿using System;
using System.IO;

namespace Compiler
{
    public static class FileReaderWriter
    {
        public static string ReadFile(string fileName)
        {
            return File.ReadAllText(fileName);
        }
        
        public static void WriteFile(string fileName, string data)
        {
           File.WriteAllText(fileName, data);
        }
    }
}