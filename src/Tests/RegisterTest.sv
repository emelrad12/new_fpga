module RegisterTest(clk, result);
input wire clk;
output reg result;
reg[2:0][4:0] location;
reg[2:0][31:0] in;
reg[2:0] isWrite;
reg[2:0][31:0] out;
Register register(clk, location, in, isWrite, out);
always @(posedge clk)begin 
	location[0] = 1;
	location[1] = 5;
	location[2] = 1;
	in[0] = 15;
	in[1] = 66;
	isWrite[0] = 1;
	isWrite[1] = 1;
	result = out[2] ==  15;
end

endmodule