module CpuCore(input wire clk, output wire LED1);
    `include "./../Globals.sv"
    //register
    reg[2:0][4:0] regLocation;
    reg[2:0][31:0] regIn;
    reg[2:0] regIsWrite;
    reg[2:0][31:0] regOut;
    Register register(clk, regLocation, regIn, regIsWrite, regOut);
    //decoder
    reg[31:0] instruction;
    reg[4:0] decoderOpCode;
    reg[4:0] regDecoderA, regDecoderB, regDecoderC;
    reg[15:0] decoderConstant;
    reg decoderIsConstant, decoderIsMemLoad, decoderIsMemStore, decoderisAluOp, decoderIsBranch;
    reg[1:0] decoderBranchType;
    Decoder decoder(instruction, decoderOpCode, regDecoderA, regDecoderB, regDecoderC, decoderConstant, decoderIsConstant, decoderIsMemLoad, decoderIsMemStore, decoderisAluOp, decoderIsBranch, decoderBranchType);
    //alu
    reg[31:0] aluInputA, aluInputB;
    reg[31:0] aluOut;
    ALU alu(clk, decoderOpCode, aluInputA, aluInputB, aluOut);
    //RAM
    reg[1:0][12:0] ramLocation;
    reg[1:0][31:0] ramInputs;
    reg[1:0] ramIsWrite = 0;
    reg[1:0][31:0] ramOutputs;
    RAM ram(ramLocation[0], ramLocation[1], clk, ramInputs[0], ramInputs[1], ramIsWrite[0], ramIsWrite[1], ramOutputs[0], ramOutputs[1]);

    byte stage = 0;
    shortint PC = 0;

    always @(posedge clk)begin
        case(stage)
            0:begin
                //Load instruction At PC and reset writes
					ramLocation[0] <= PC;
            end
            3:begin
                //Decode
					instruction <= ramOutputs[0];
            end
            4:begin
                //load
                regIsWrite[0] <= 0;
                regIsWrite[1] <= 0;
					 regIsWrite[2] <= 0;
                regLocation[0] <= regDecoderA;
                regLocation[1] <= regDecoderB;
					 regLocation[2] <= regDecoderC;
            end
            6:begin
                //store in memory
                if(decoderIsBranch == 1)begin
                    stage+=3;//needs only 1 cycle;
                    regIsWrite[1] <= 1;
                    regLocation[1] <= regDecoderC;
                    regIn[1] <= PC + 1;
                    case(decoderBranchType)
                        0:begin
                            if(regOut[0] == 0)
                                PC += decoderConstant;
                        end
                        1:begin
                            if(regOut[0] == 1)
                                PC += decoderConstant;
                        end
                        2:begin
                            PC = regOut[0];
                        end
                    endcase
                end
                if(decoderIsMemStore == 1)begin
                    ramIsWrite[0] <= 1;
                    ramLocation[0] <= regOut[0] + decoderConstant;
						  ramInputs[0] <= regOut[2];
                end
                //load from memory
                if(decoderIsMemLoad == 1)begin
                    ramIsWrite[0] <= 0;
                    ramLocation[0] <= regOut[0] + decoderConstant;
                end
                //alu
                if(decoderisAluOp == 1)begin
                    stage++;// needs only 2 cycles instead of 3 so skip one
                    aluInputA <= regOut[0];
                    aluInputB <= decoderIsConstant == 0 ? regOut[1] : decoderConstant;
                end
            end
            9:begin 
                //the store op continues to the 9th cycle 
                //increment PC
                PC += 1;
                if(decoderIsMemLoad == 1)begin
                    regIsWrite[0] <= 1;
                    regLocation[0] <= regDecoderC;
                    regIn[0] <= ramOutputs[0];
                end
                //alu
                if(decoderisAluOp == 1)begin
                    regIsWrite[0] <= 1;
                    regLocation[0] <= regDecoderC;
                    regIn[0] <= aluOut;
                end
            end
        endcase
        stage++;
        if(stage == 15)begin
            stage <= 0;
        end
    end
endmodule