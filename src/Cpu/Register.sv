module Register(clk ,location, in, isWrite, out);
	input wire clk;
	input wire[2:0][4:0] location;
	input wire[2:0][31:0] in;
	input wire[2:0] isWrite;
	output reg[2:0][31:0] out;
	reg[31:0][31:0] memory;

	initial begin
		for(int i = 0; i <32; i++)begin
			memory[i] = 0;
		end
	end
	reg[1:0] i;
	always @(posedge clk)begin 
		for(i = 0; i<=2; i++)begin 
			if(isWrite[i] == 1)begin 
				memory[location[i]] = in[i];
			end
			out[i] = memory[location[i]];		 
		end
		memory[31] <= 0;
	end
endmodule