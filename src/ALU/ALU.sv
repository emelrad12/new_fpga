module ALU(clk, opCode, inputA, inputB, out);
input wire[4:0] opCode;
input wire clk;
input wire[31:0] inputA, inputB;
output reg[31:0] out;

always @(posedge clk)begin
	out = 0;
	case(opCode)

		5'b00100: begin out = inputA + inputB; end
		5'b01000: begin out = inputA - inputB; end
		5'b01100: begin out = inputA * inputB; end
		5'b10000: begin out = inputA / inputB; end
		5'b10100: begin out = inputA == inputB; end
		5'b11000: begin out = inputA < inputB; end
		5'b11100: begin out = inputA <= inputB; end

		5'b00101: begin out = inputA & inputB; end
		5'b01001: begin out = inputA | inputB; end
		5'b01101: begin out = inputA ^ inputB; end
		5'b10001: begin out = inputA ~^ inputB; end
		5'b10101: begin out = inputA << inputB; end
		5'b11001: begin out = inputA >> inputB; end

	endcase
end

endmodule