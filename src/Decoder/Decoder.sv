module Decoder(inputInstruction, opCode, regA, regB, regC, constant, isConstant, isMemLoad, isMemStore, aluOp, isBranch, branchType);
    input wire[31:0] inputInstruction;
    //last bit of inputInstruction opCode is used to represed isConstant 
    //which is not relevant to the ALU, thus it is separated here;
    output wire[4:0] opCode;
    output wire[4:0] regA, regB, regC;
    output wire[15:0] constant;
    output wire isConstant, isMemLoad, isMemStore, aluOp, isBranch;
    output wire[1:0] branchType;

    assign opCode = inputInstruction[31:27];
    assign regC = inputInstruction[25:21];
    assign regA = inputInstruction[20:16];
    assign regB = inputInstruction[15:11];
    assign constant = inputInstruction[15:0];
    assign isConstant = inputInstruction[26];
    assign aluOp = opCode[1] == 0;
    assign isMemLoad = opCode == 5'b00010;
    assign isMemStore = opCode == 5'b00110;
    assign isBranch = opCode[1:0] == 2'b11;
    assign branchType = opCode[3:2];
endmodule